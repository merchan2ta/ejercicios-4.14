
# Crear un algoritmo que presente una calificacion
# de acuerdo a ciertas equivalentes
# Crear una funcion llamada calculo_calificacion
# Autor : Pedro Vicente Merchan Infante
# Email__ pedro.merchan@unl.edu.ec


def calcula_calificacion(a):

    if a >= 0  and a  <=  1.0:
        if a >=  0.9:
            print("Sobresaliente")
        elif a >=  0.8:
            print("Notable")
        elif a >=  0.7:
            print("Bien")
        elif a <=  0.6:
            print("Insuficiente")
    else:
        print("Fuera del rango")


try:
    puntuacion  =   float(input("Introduzca la puntuacion:\n"))
    calcula_calificacion(puntuacion)
except:
    print("Puntuacion Incorrecta")
