
#   crear un algoritmo donde si la tarifa sobrepasa de 40 se le dara al
#   cliente 1.5 veces mas
#   Realizar un algoritmo cuya función sea Crear una funcion llamada calculo_salario
# Autor : Pedro Vicente Merchan Infante
# Email__ pedro.merchan@unl.edu.ec

def calculo_salario (s,t):
    if s <= 40:
        total = s * t
        print("El valor de su sueldo es ", total, " dolares")
    elif s >= 41:
        s   -=  40
        total  =   40 * t
        incre  =   1.5 * (s * t)
        total +=  incre
        print("El valor que adquiere es de ", total, " dolares")
    else:
        print("La cantidad que digitó es incorrecta")

try:
    saldo   =   float(input("Ingrese el numero de horas trabajadas:\n"))

    tarifa  =   float(input("Engrese el monto por hora:\n"))

    calculo_salario(saldo,tarifa)

except:
    print("La cantidad que Usted ingreso es incorrecta")
